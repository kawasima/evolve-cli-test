FROM mcr.microsoft.com/dotnet/runtime-deps:5.0-buster-slim-amd64

ADD https://github.com/lecaillon/Evolve/releases/download/2.4.0/evolve_2.4.0_Linux-64bit.tar.gz evolve.tar.gz
RUN tar xzf evolve.tar.gz && mv evolve_2.4.0_Linux-64bit/evolve /usr/local/bin && rm -rf /evolve_2.4.0_Linux-64bit /evolve.tar.gz
COPY migrations/ /migrations/
ENTRYPOINT evolve migrate postgresql -c "server=local_dev;User Id=postgres;Password=password;" -l /migrations
