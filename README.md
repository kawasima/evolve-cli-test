# Evolveを使ったデータベースマイグレーション

ASP.NET coreのデータベースマイグレーションは、EFCoreの機能を使うと思うが、SQLファイルベースで世代管理するには、本番運用を考えると使い勝手が良くない。
そこでJavaのFlyway互換であるEvolveを使ってマイグレーションを行うやり方を、ここに示す。

## 使い方

evolve-cliを含むコンテナを作る

```
% docker build -t db-migration
```

コンテナを実行する

```
% docker run -it -rm --add-host=[DBサーバのIPアドレス] db-migration
```
